#CLI Controller
class ToDoList::CLI

  def call
    displayList
    menu
    goodbye
  end

  def displayList
    puts "Things to do: "
    @items = ToDoList::List.all
    @items.each_with_index do |item, i|
      puts "#{i+1}. #{item.title} - Due Date:#{item.due_date} - Priority:#{item.priority}"
      puts
    end
  end

  def menu
    input = nil
    while input != 'exit'
      puts "Enter the index of the item you wish to know more about or type 'list' to see the list again. Type 'exit' when you are finished."
      input = gets.strip.downcase

      if input.to_i > 0 && input.to_i < @items.length + 1
        the_item = @items[input.to_i-1]
        puts "#{the_item.title} - #{the_item.summary}"
      elsif input == "list"
        displayList
      elsif input == "exit"
        break
      else
        puts "I'm unsure what you meant. Please enter a valid input."
        puts
      end
    end
  end

  def goodbye
    puts "Get to Work!"
  end


end
