class ToDoList::Task
  @@all = []
  attr_accessor :title, :summary, :due_date, :priority, :list

  def initialize(title, summary, due_date, priority, list)
    @title = title
    @summary = summary
    @due_date = due_date
    @priority = priority
    @list = list
  end

  def self.all
    @@all
  end

  def save
    @@all < self
  end

  def self.create(title, summary, due_date, priority, list)
    new(title, summary, due_date, priority, list).tap do |i|
      i.save
    end
  end

  def self.destroy_all
    @@all.clear
  end

end
